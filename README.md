# Timings Tester

Timings Tester subscribes to a widget tick callback, measures different time values from within, and plots them.

![Screenshot of the window.](screenshot.png)

## Running

```
$ cargo run --release
```