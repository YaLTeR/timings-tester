use gtk::prelude::*;

mod tester;
use tester::Tester;

fn main() {
    let app = gtk::Application::builder().build();

    app.connect_activate(build_ui);

    app.run();
}

fn build_ui(app: &gtk::Application) {
    let window = gtk::ApplicationWindow::builder()
        .application(app)
        .title("Timings Tester")
        .default_width(800)
        .default_height(800)
        .build();

    let popover = gtk::Popover::new();
    popover.set_child(Some(&gtk::Label::new(Some("Hello World"))));
    let button = gtk::MenuButton::new();
    button.set_popover(Some(&popover));

    let headerbar = gtk::HeaderBar::new();
    headerbar.pack_end(&button);
    window.set_titlebar(Some(&headerbar));

    let tester = Tester::new();
    window.set_child(Some(&tester));

    window.present();
}
