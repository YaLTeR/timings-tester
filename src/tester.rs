use gtk::glib;
use gtk::subclass::prelude::*;

mod imp {
    use std::{
        cell::RefCell,
        time::{Duration, Instant},
    };

    use circular_queue::CircularQueue;
    use gtk::{gdk, graphene, pango, prelude::*};

    use super::*;

    #[derive(Debug, Clone)]
    struct State {
        flip_color: bool,

        last_call_time: Option<Instant>,
        call_timings: CircularQueue<Duration>,
        call_layout: Option<pango::Layout>,
        last_frame_time: Option<Duration>,
        frame_timings: CircularQueue<Duration>,
        frame_layout: Option<pango::Layout>,
        last_predicted_presentation_time: Option<Duration>,
        presentation_timings: CircularQueue<Duration>,
        presentation_layout: Option<pango::Layout>,

        low_layout: Option<pango::Layout>,
        high_layout: Option<pango::Layout>,
        high: i32,
    }

    #[derive(Debug, Default)]
    pub struct Tester {
        state: RefCell<State>,
    }

    impl Default for State {
        fn default() -> Self {
            Self {
                flip_color: false,
                last_call_time: None,
                call_timings: CircularQueue::with_capacity(10000),
                last_frame_time: None,
                frame_timings: CircularQueue::with_capacity(10000),
                last_predicted_presentation_time: None,
                presentation_timings: CircularQueue::with_capacity(10000),
                low_layout: None,
                high_layout: None,
                high: 10,
                call_layout: None,
                frame_layout: None,
                presentation_layout: None,
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Tester {
        const NAME: &'static str = "TTTester";
        type Type = super::Tester;
        type ParentType = gtk::Widget;
    }

    impl ObjectImpl for Tester {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.add_tick_callback(|obj, clock| Self::from_instance(obj).tick_callback(obj, clock));
        }
    }

    impl WidgetImpl for Tester {
        fn snapshot(&self, widget: &Self::Type, snapshot: &gtk::Snapshot) {
            let mut state = self.state.borrow_mut();

            let width = widget.allocated_width();
            let height = widget.allocated_height();

            // Work around a rectangle diffing performance issue by changing the background color
            // every frame.
            //
            // https://gitlab.gnome.org/GNOME/gtk/-/issues/4560
            let value = if state.flip_color { 0.999 } else { 1. };
            state.flip_color = !state.flip_color;

            snapshot.append_color(
                &gdk::RGBA::new(value, value, value, 1.),
                &graphene::Rect::new(0., 0., width as f32, height as f32),
            );

            // Update the high value.
            if let Some(high) = state
                .call_timings
                .iter()
                .take(width as usize)
                .chain(state.frame_timings.iter().take(width as usize))
                .chain(state.presentation_timings.iter().take(width as usize))
                .max()
            {
                let high = i32::try_from(high.as_millis() / 10 * 10 + 10).unwrap();
                if state.high != high {
                    state.high = high;
                    state
                        .high_layout
                        .as_ref()
                        .unwrap()
                        .set_text(&format!("{} ms", high));
                }
            }

            let block_height = height / 3;
            self.draw_timings(
                widget,
                snapshot,
                width,
                block_height,
                &*state,
                &state.call_timings,
                &gdk::RGBA::new(1., 0., 0., 1.),
                state.call_layout.as_ref().unwrap(),
            );

            snapshot.translate(&graphene::Point::new(0., block_height as f32));
            self.draw_timings(
                widget,
                snapshot,
                width,
                block_height,
                &*state,
                &state.frame_timings,
                &gdk::RGBA::new(0., 0.7, 0., 1.),
                state.frame_layout.as_ref().unwrap(),
            );

            snapshot.translate(&graphene::Point::new(0., block_height as f32));
            self.draw_timings(
                widget,
                snapshot,
                width,
                block_height,
                &*state,
                &state.presentation_timings,
                &gdk::RGBA::new(0.2, 0.2, 1., 1.),
                state.presentation_layout.as_ref().unwrap(),
            );
        }

        fn root(&self, widget: &Self::Type) {
            self.parent_root(widget);

            let mut state = self.state.borrow_mut();
            state.call_layout =
                Some(widget.create_pango_layout(Some("time at tick callback call")));
            state.frame_layout =
                Some(widget.create_pango_layout(Some("gdk_frame_clock_get_frame_time()")));
            state.presentation_layout =
                Some(widget.create_pango_layout(Some(
                    "gdk_frame_timings_get_predicted_presentation_time()",
                )));
            state.low_layout = Some(widget.create_pango_layout(Some("0 ms")));
            state.high_layout =
                Some(widget.create_pango_layout(Some(&format!("{} ms", state.high))));
        }
    }

    impl Tester {
        fn tick_callback(&self, obj: &super::Tester, clock: &gdk::FrameClock) -> glib::Continue {
            let time = Instant::now();
            let frame_time = Duration::from_micros(clock.frame_time().try_into().unwrap());
            let predicted_presentation_time = Duration::from_micros(
                clock
                    .current_timings()
                    .unwrap()
                    .predicted_presentation_time()
                    .try_into()
                    .unwrap(),
            );

            let mut state = self.state.borrow_mut();

            if let Some(last_call_time) = state.last_call_time {
                let delta = time.duration_since(last_call_time);
                state.call_timings.push(delta);
            }
            state.last_call_time = Some(time);

            if let Some(last_frame_time) = state.last_frame_time {
                let delta = frame_time - last_frame_time;
                state.frame_timings.push(delta);
            }
            state.last_frame_time = Some(frame_time);

            if let Some(last_predicted_presentation_time) = state.last_predicted_presentation_time {
                let delta = predicted_presentation_time
                    .checked_sub(last_predicted_presentation_time)
                    .unwrap_or_default();
                state.presentation_timings.push(delta);
            }
            state.last_predicted_presentation_time = Some(predicted_presentation_time);

            obj.queue_draw();
            glib::Continue(true)
        }

        #[allow(clippy::too_many_arguments)]
        fn draw_timings(
            &self,
            obj: &super::Tester,
            snapshot: &gtk::Snapshot,
            width: i32,
            height: i32,
            state: &State,
            timings: &CircularQueue<Duration>,
            color: &gdk::RGBA,
            description: &pango::Layout,
        ) {
            // Draw the timings.
            if !timings.is_empty() {
                let high = (state.high as f32) / 1000.;
                for (x, delta) in (0..width).rev().zip(timings.iter()) {
                    let bar_height = (delta.as_secs_f32() / high).min(1.) * height as f32;

                    snapshot.append_color(
                        color,
                        &graphene::Rect::new(x as f32, height as f32 - bar_height, 1., bar_height),
                    );
                }
            }

            // Draw the grid.
            for level in 1.. {
                let subdivision = 2i32.pow(level);
                let step = height / subdivision;
                if step < 100 {
                    break;
                }

                if step < 200 {
                    for i in 1..subdivision {
                        let y = height * i / subdivision;
                        snapshot.append_color(
                            &gdk::RGBA::new(0.9, 0.9, 0.9, 1.),
                            &graphene::Rect::new(0., y as f32, width as f32, 1.),
                        );
                    }

                    break;
                }
            }

            // Draw the labels.
            let layout = state.low_layout.as_ref().unwrap();
            snapshot.render_layout(
                &obj.style_context(),
                0.,
                (height - layout.pixel_size().1) as f64,
                layout,
            );
            let layout = state.high_layout.as_ref().unwrap();
            snapshot.render_layout(&obj.style_context(), 0., 0., layout);

            let layout = description;
            snapshot.render_layout(
                &obj.style_context(),
                (width - layout.pixel_size().0) as f64,
                0.,
                layout,
            );
        }
    }
}

glib::wrapper! {
    pub struct Tester(ObjectSubclass<imp::Tester>) @extends gtk::Widget;
}

impl Tester {
    pub(crate) fn new() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}
